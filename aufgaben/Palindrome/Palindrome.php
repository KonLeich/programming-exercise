<?php

declare(strict_types=1);

namespace aufgaben\Palindrome;

/**
 * Ein Palindrom ist ein String, der rückwärts gelesen denselben String ergibt.
 * Nich-alphanumerische Zeichen müssen ebenfalls unterstützt werden.
 *
 * @method static bool check(string $string)
 * @example Palindrome::check('asddsa')  === true
 * @example Palindrome::check('asdd')  === false
 */
final class Palindrome{
	public $var ="ein wert";
	//public  bool check( STring Str){
		
	public static function check(string $str1):bool{
		$dtype=gettype($str1);

		if($dtype=="string"){
		//	$str1 = strtolower($str1);
		//	$str1=str_replace(" ", "",$str1);
			$str2=strrev($str1);
		
			if($str1==$str2){
				return True;
			}else{

				return False;
			}			

		}else{
			return False;
		}
	}
}


?>

