<?php

declare(strict_types=1);

namespace Aufgaben\Reverse;

/**
 * Erstelle Methoden, die den gegebenen Input umkehren.
 *
 * @method static int int(int $number)
 * @method static string string(string $string)
 * @example Reverse::int(12) === 21
 * @example Reverse::int(912) === 219
 * @example Reverse::int(120) === 21
 * @example Reverse::int(-12) === -21
 * @example Reverse::int(-120) === -21
 * @example Reverse::string('qwerty')  === 'ytrewq'
 * @example Reverse::string('apple')  === 'elppa'
 */
 

final class Reverse
{

 	public static function int(int $number):int{
		if($number<0){
			$vorzeichen=-1;
		}else{
			$vorzeichen=1;
		}
		$asstring=(string) abs($number);
		$number=$vorzeichen*(int)strrev($asstring);
		return $number;
		
 	}
 
  	public static function string(string $string):string{
	  	return	strrev($string);
 	}


}

?>
