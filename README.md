<img src="https://www.mpib-berlin.mpg.de/assets/institutes/headers/mpib-desktop-de-ac818ca81dc870085876d6f0caabbb7966a434c5747b8675582d116aca4c4c8c.svg"
     alt="MPIB-Berlin"
     style="float: left; margin-right: 10px;" />

# MPIB-Berlin Programmieraufgaben für Berwerber:innen

Herzlich Willkommen und vielen Dank für Ihr Interesse an einer Stelle zur wissenschaftlichen/studentischen Hilfskraft als Programmierer:in im Fachbereich Adapative Rationalität am Max-Planck-Instutit für Bilfungsforschung.

Nachfolgend finden Sie zwei Aufgabenbereiche. Im ersten Aufgabenbereich geht es das Erstellen eines Algorithmus zur Lösung eines konkreten Problems. Im zweiten Aufgabenbereich geht es um die Erstellung eines HTML-Formulars sowie der damit verbundenen Datenspeicherung in einer Datenbank und verfolgt daher einen eher praxisorientierten Ansatz.

### Technische Voraussetzungen
Um Ihre Lösungen selbst testen zu können empfiehlt sich die Installation von PHP und Composer auf Ihrem Rechner. Sollten Sie Fragen hierzu haben, kontaktieren Sie uns gerne.

### Bevor Sie beginnen
Bitte forken Sie dieses Repository in Ihren persönlichen Namespace. Räumen Sie den Accounts @philjak und @messersm mindestens Developer-Rechte ein, um den Code sowie die CI-Pipelines zu bzw. zu starten.

### Abgabe
Spätestens 24 Stunden nachdem Sie diesen Link bzw. die Einladung per E-Mail erhalten haben, bitten wir Sie Ihre Lösungen in Ihr forked Repository zu speichern. 

## Aufgabenbereich 1
Werfen Sie einen Blick in den Ordner `/aufgaben`. Hier gibt es **5** verschiedene Aufgaben, von denen Sie sich mind. **2** Aufgaben zur Bearbeitung aussuchen können. Innerhalb der entsprechenden PHP-Datei erhalten Sie eine kurze Erklärung sowie Bespiele zur Funktionaltität. Ziel ist es, die gewünschte Funktionalität zu implementieren.

### Ablauf
- Klonen Sie dieses Repository auf Ihren Rechner.

- Installieren Sie mit `composer install` alle notwendigen Abhängigkeiten.

- Wählen Sie mind. zwei Aufgaben zur Implementierung und entfernen Sie in den zugehörigen Tests alle Vorkommnisse von `self::markTestSkipped();`

- Um Ihre Lösungen selbst zu testen, geben Sie in der Konsole ein: `./vendor/bin/phpunit`.

- Pushen Sie Ihre Ergbnisse in dieses Repository.

- Wenn Sie fertig sind, geben Sie dem finalen Commit einen entsprechenden Tag. Falls Sie nachträglich noch eine Änderung durchführen, vergessen Sie nicht einen neuen Tag zu setzen.


## Aufgabenbereich 2
- Erstellen Sie einen Ordner `public` im Root dieses Repositories sowie darin eine Datei mit den Namen `index.php`. Diese Datei soll eine Benutzerregistrierung darstellen und dazu ein HTML-Formular mit den folgenden Input-Feldern anzeigen:
  - Benutzername
  - Passwort
  - Passwort (nochmals)
  <br><br>
- Erstellen Sie eine PHP-Datei mit beliebigen Namen, die die Formulardaten entgegen nimmt.
    - *Optional:* Prüfen Sie, ob die Passwörter identisch sind. Sowohl PHP als auch JavaScript sind erlaubt.
    - *Optional:* Nutzen Sie das Hashverfahren md5, um das Kennwort in der Datenbank zu speichern.
<br><br>
- Erstellen Sie eine Datenbanktabelle mit den notwendigen Feldern, um die Daten der Benutzerregistrierung zu speichern.


- Speichern Sie die Formulardaten in der Datenbank.

- *Optional:* Sobald das Formular abgesendet ist, zeigen Sie einen Dankestext mit der aktuellen Uhrzeit an. Nutzen Sie zum Auslesen der aktuellen Uhrzeit nicht die Systemzeit, sondern die folgende API: http://worldclockapi.com/api/json/cet/now Diese API liefert ein JSON zurück. Der notwendige Wert steht im Key `currentDateTime`.

- Schreiben Sie Ihren Code und einen SQL Dump zurück (Push) in den eben erstellen `public` Ordner.
